## CHANGELOG

### v0.2.4
> Moving this permanently to [Github: https://github.com/NYPL/nypl-svg-icons](https://github.com/NYPL/nypl-svg-icons)
***Please make sure you are using that repo, as this one is no longer maintained***

### v0.2.3
> Added FB, Twitter and a new Apply Icon, changed the _old_ Apply icon to a Download icon

### v0.2.2
> (╯°□°）╯︵ ┻━┻

### v0.2.1
> Added attribute updates to <ApplyIcon> and <ResetIcon>.

### v0.2
> Removed hard coded `fill` attributes

### v0.1.0
> Updating to React 15.
> Making sure packages all have an exact version in package.json.

#### v.0.23
> Added WedgeDownIcon,

#### v.0.22
> rolled back to avoid React complaints about missing iconId prop.
> TODO: change all relevant icons to use this prop, and then make changes across the apps.

#### v.0.21
> Accessibility updates as well as added `preserveAspectRatio` to XIcon's attribute set
> Added Changeloging to this reamdme.md file.

#### 0.0.20
> Accessibility updates to login / logout icons
